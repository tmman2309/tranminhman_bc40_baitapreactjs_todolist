import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Container } from "../../JSS_StyledComponent/Components/Container";
import { Dropdown } from "../../JSS_StyledComponent/Components/Dropdown";
import { Heading3 } from "../../JSS_StyledComponent/Components/Heading";
import { TextField } from "../Components/TextField";
import { Button } from "../../JSS_StyledComponent/Components/Button";
import { Table, Th, Thead, Tr } from "../Components/Table";
import { connect } from "react-redux";
import {
  addTaskAction,
  changThemeAction,
  deleteTaskAction,
  doneTaskAction,
  editTaskAction,
  redoTaskAction,
  updateTaskAction,
} from "../../redux/actions/toDoListActions";
import { arrTheme } from "../Themes/ThemeManager";

class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
    disabledAddTask: false,
  };

  renderTaskToDo = () => {
    // filter !task.done có nghĩa là filter task.done == false
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.setState(
                    {
                      disabled: false,
                      disabledAddTask: true,
                    },
                    () => {
                      this.props.dispatch(editTaskAction(task));
                    }
                  );
                }}
                className="ml-1"
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskCompleted = () => {
    // filter task.done có nghĩa là filter task.done == true
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(redoTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-redo"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return (
        <option key={index} value={theme.id}>
          {theme.name}
        </option>
      );
    });
  };

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <div>
          <Container className="w-75 my-5">
            {/* --SELECT THEME-- */}
            <Dropdown
              className="mb-3"
              onChange={(e) => {
                let { value } = e.target;

                this.props.dispatch(changThemeAction(value));
              }}
            >
              {this.renderTheme()}
            </Dropdown>

            {/* --TO DO LIST-- */}
            <Heading3 className="py-2">To Do List</Heading3>
            <TextField
              value={this.state.taskName}
              onChange={(e) => {
                this.setState({
                  taskName: e.target.value,
                });
              }}
              name="taskName"
              className="w-50"
              label="Task Name: "
            />
            {this.state.disabledAddTask ? (
              <Button disabled className="ml-2">
                <i className="fa fa-plus mr-1"></i>Add task
              </Button>
            ) : (
              <Button
                onClick={() => {
                  // Lấy thông tin người dùng nhập từ Input
                  let { taskName } = this.state;
                  // Tạo ra 1 task object
                  let newTask = {
                    id: Date.now(),
                    taskName,
                    done: false,
                  };
                  // Đưa task object lên redux thông qua phương thức Dispatch
                  this.props.dispatch(addTaskAction(newTask));
                }}
                className="ml-2"
              >
                <i className="fa fa-plus mr-1"></i>Add task
              </Button>
            )}
            {this.state.disabled ? (
              <Button disabled className="ml-2">
                <i className="fa fa-upload mr-1"></i>Update task
              </Button>
            ) : (
              <Button
                onClick={() => {
                  // Lưu lại để khi dispatch thì taskName không bị rỗng, vì mình đã cho setState chạy trước
                  let { taskName } = this.state;
                  this.setState(
                    {
                      disabled: true,
                      disabledAddTask: false,
                      taskName: "",
                    },
                    () => {
                      this.props.dispatch(updateTaskAction(taskName));
                    }
                  );
                }}
                className="ml-2"
              >
                <i className="fa fa-upload mr-1"></i>Update task
              </Button>
            )}

            <hr className="bg-white" />

            {/* --TASK TO DO-- */}
            <Heading3 className="py-2">Task To Do</Heading3>
            <Table>
              <Thead>{this.renderTaskToDo()}</Thead>
            </Table>

            {/* --TASK COMPLETED-- */}
            <Heading3 className="py-2">Task Completed</Heading3>
            <Table>
              <Thead>{this.renderTaskCompleted()}</Thead>
            </Table>
          </Container>
        </div>
      </ThemeProvider>
    );
  }

  // Đây là lifecycle trả về props cũ và state cũ của component trước khi render (Lifecycle chạy sau render)
  componentDidUpdate(prevProps, prevState) {
    // Phải có điều kiện so sánh để chạy setState, tránh tình trạng vòng lặp vô hạn
    // Phép so sánh: Nếu như props trước đó (taskEdit trước mà khác taskEdit hiện tại thì mới setState)
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.toDoListReducer.themeToDoList,
    taskList: state.toDoListReducer.taskList,
    taskEdit: state.toDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
