import { ToDoListDarkTheme } from "./ToDoListDarkTheme";
import { ToDoListLightTheme } from "./ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "./ToDoListPrimaryTheme";

export const arrTheme = [
  {
    id: 1,
    name: "Select: Dark Theme",
    theme: ToDoListDarkTheme,
  },
  {
    id: 2,
    name: "Select: Light Theme",
    theme: ToDoListLightTheme,
  },
  {
    id: 3,
    name: "Select: Primary Theme",
    theme: ToDoListPrimaryTheme,
  },
];
