import './App.css';
import ToDoList from './JSS_StyledComponent/ToDoList/ToDoList';
import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  body {
    background-color: #f2f2f2;
  }
`;

function App() {
  return (
    <div>
      <GlobalStyle />
      <ToDoList />
    </div>
  );
}

export default App;
