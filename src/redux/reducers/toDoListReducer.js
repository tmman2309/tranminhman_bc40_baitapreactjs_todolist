import { arrTheme } from "../../JSS_StyledComponent/Themes/ThemeManager";
import { ToDoListDarkTheme } from "../../JSS_StyledComponent/Themes/ToDoListDarkTheme";
import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  REDO_TASK,
  UPDATE_TASK,
} from "../constants/toDoListConstants";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: "task-1", taskName: "task 1", done: false },
    { id: "task-2", taskName: "task 2", done: true },
  ],
  taskEdit: { id: "-1", taskName: "", done: false },
};

export default (state = initialState, action) => {
  switch (action.type) {
    // ADD TASK
    case ADD_TASK: {
      // Kiểm tra rỗng
      if (action.newTask.taskName.trim() === "") {
        alert("Task name is required!");
        return { ...state };
      }
      // Kiểm tra tồn tại
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => {
        return task.taskName === action.newTask.taskName;
      });
      if (index !== -1) {
        alert("Task name already exists!");
        return { ...state };
      }

      taskListUpdate.push(action.newTask);
      return { ...state, taskList: taskListUpdate };
    }

    // CHANGE THEME
    case CHANGE_THEME: {
      let theme = arrTheme.find((theme) => {
        return theme.id == action.themeID;
      });

      if (theme) {
        return { ...state, themeToDoList: { ...theme.theme } };
      }
    }

    // DONE TASK
    case DONE_TASK: {
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => {
        return task.id === action.taskID;
      });

      if (index !== -1) {
        taskListUpdate[index].done = true;
      }

      return { ...state, taskList: taskListUpdate };
    }

    // DELETE TASK
    case DELETE_TASK: {
      let taskListUpdate = [...state.taskList];
      taskListUpdate = taskListUpdate.filter((task) => {
        return task.id !== action.taskID;
      });

      return { ...state, taskList: taskListUpdate };
    }

    // REDO TASK
    case REDO_TASK: {
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => {
        return task.id === action.taskID;
      });

      if (index !== -1) {
        taskListUpdate[index].done = false;
      }

      return { ...state, taskList: taskListUpdate };
    }

    // EDIT TASK
    case EDIT_TASK: {
      return { ...state, taskEdit: action.task };
    }

    // UPDATE TASK
    case UPDATE_TASK: {
      // Chỉnh sửa taskName của taskEdit
      state.taskEdit = { ...state.taskEdit, taskName: action.taskName };

      // Tìm trong taskList và cập nhật lại task
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => {
        return task.id === state.taskEdit.id;
      });
      let checkName = taskListUpdate.findIndex((task) => {
        return task.taskName === state.taskEdit.taskName;
      });

      if (index !== -1) {
        if (checkName === -1) {
          taskListUpdate[index] = state.taskEdit;
        } else {
          alert("Task name already exists!");
        }
      }

      state.taskEdit = { id: "-1", taskName: "", done: false };

      return { ...state, taskList: taskListUpdate };
    }

    default:
      return state;
  }
};
