import { ADD_TASK, CHANGE_THEME, DELETE_TASK, DONE_TASK, EDIT_TASK, REDO_TASK, UPDATE_TASK } from "../constants/toDoListConstants";

export const addTaskAction = (newTask) => ({
  type: ADD_TASK,
  newTask
})

export const changThemeAction = (themeID) => ({
  type: CHANGE_THEME,
  themeID
})

export const doneTaskAction = (taskID) => ({
  type: DONE_TASK,
  taskID
})

export const deleteTaskAction = (taskID) => ({
  type: DELETE_TASK,
  taskID
})

export const redoTaskAction = (taskID) => ({
  type: REDO_TASK,
  taskID
})

export const editTaskAction = (task) => ({
  type: EDIT_TASK,
  task
})

export const updateTaskAction = (taskName) => ({
  type: UPDATE_TASK,
  taskName
})

